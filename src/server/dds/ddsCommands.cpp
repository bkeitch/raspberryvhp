#include "ddsDevice.h"
#include "ddsCommands.h"
#define firmwareVerNum (1.0)
#define MAX_CHANNEL 4 //BCK 2014.06.25
const char terminators []  = " \t\f\v\r\n";
void error(int why, char *where, char* reason) {
    if(0==why) return;
    const char* reasons[] = {
        "","channel out of range", "no set parameter specified",
        "unrecognised argument", "unexpected garbage at EOF", "parameter is outside DAC range", "read timeout", "clock overflow during read",
        "buffer overflow during read", "no command found", "unrecognised command" , "unrecognised channel"  };
    snprintf(reason, 100, "Error : %s : %s ", where, reasons[why]);
}



/*
 * parses our command and returns 0 if no error, or an error code otherwise
 ** BCK
 */
int parseCommand(unsigned int *channel, double *parameter) {
    char tempBuffer [1];
    char *pos = strtok(NULL, terminators);
    if ('\0' == pos)
    {
        return(11);
    }
    if(1 != sscanf(pos, "%u%s", channel, tempBuffer))    // Extra argument checks for remaining characters (e.g. will give an error if we pass "2a" as a voltage)
    {
        return(3);
    }
    if(MAX_CHANNEL <= *channel)                                    // N.B. First DAC is 0
    {
        return(1);
    }

    if (parameter != 0) {

        pos = strtok(NULL, terminators);
        if('\0' == pos)
        {
            return (2);
        }
        char *endOfDouble;
        *parameter = strtod(pos, &endOfDouble);
        if(NULL == endOfDouble || (strlen(pos) != (sizeof(char)*(endOfDouble - pos))) )
        {
            return(3);
        }

        pos = strtok(NULL, terminators);
        if('\0' != pos)
        {
            return(4);
        }
    }
    return(0);
}
void identity(char * buffer)
{
    snprintf(buffer, 50,"Raspberry Pi powered DDS Board. Firmware version V%1.1f \n", firmwareVerNum);
printf(" Asked for ID : %s \n", buffer);

}
void process(const char *command, char*result)
{

#ifdef DEBUG
    static ddsDevice* dds = new ddsDevice("/tmp/debug",12000000);
    printf("\n* Testing command %s*\n", command);
#else
    static ddsDevice* dds = new ddsDevice("/dev/ttyUSB0",9600);
#endif
    strncpy(result,"OK",2);
    char *pos;
    dds->init();
    pos = strtok((char*)command, terminators);
    if('\0' == pos)
    {
        error(9, "process", result);
        return;
    }

    if(0 == strcmp(pos, "R"))
    {
        unsigned int channel;
        int errorCode = parseCommand(&channel, (double*) 0);
        if(errorCode !=0)
            error(errorCode, "reset", result);
        else dds->init(channel, 0);
        return;
    }


    if(0 == strcmp(pos, "F"))
    {
        unsigned int channel;
        double frequency;
        int errorCode = parseCommand(&channel, &frequency);
        if(errorCode !=0)
            error(errorCode, "set frequency", result);
//        dds->setFrequency((channel+1)/2, channel%2, frequency);
        //sprintf(result, "V %u %f", channel, );
        else dds->setFrequency(channel, 0, frequency);
        return;

    }


    else if(0 == strcmp(pos, "F?"))
    {
        strncpy(result,"NOT IMPLEMENTED IN FIRMWARE",30);
        return;
    }

    else if (0 == strcmp(pos, "P"))
    {
        unsigned int channel;
        double phase;
        int errorCode = parseCommand(&channel, &phase);
        if(errorCode !=0)
            error(errorCode, "set phase", result);
        else dds->setPhase(channel, 0, (float) phase);
        return;
    }
    else if(0 == strcmp(pos, "P?"))
    {
        strncpy(result,"NOT IMPLEMENTED IN FIRMWARE",30);
        return;
    }
    else if (0 == strcmp(pos, "G"))
    {
        unsigned int channel;
        double VGA;
        int errorCode = parseCommand(&channel, &VGA);
        if(errorCode !=0)
            error(errorCode, "set VGA", result);
        else dds->setVGA(channel, 0, VGA);
        return;
    }
    else if(0 == strcmp(pos, "G?"))
    {
        strncpy(result,"NOT IMPLEMENTED IN FIRMWARE",30);
        return;
    }else if (0 == strcmp(pos, "A"))
    {
        unsigned int channel;
        double Amplitude;
        int errorCode = parseCommand(&channel, &Amplitude);
        if(errorCode !=0)
            error(errorCode, "set Amplitude", result);
        else dds->setAmplitude(channel, 0, Amplitude);
        return;
    }
    else if(0 == strcmp(pos, "A?"))
    {
        strncpy(result,"NOT IMPLEMENTED IN FIRMWARE",30);
        return;
    }

    else if(0 == strcmp(pos, "UPDATE"))
    {
        unsigned int channel;
        int errorCode = parseCommand(&channel, (double*)0);
        if(errorCode !=0)
            error(errorCode, "update", result);
        else dds->update(channel, 0);
        return;
    }
    else if(0 == strcmp(pos, "POWERDOWN"))
    {
        unsigned int channel;
        int errorCode = parseCommand(&channel, (double*)0);
        if(errorCode !=0)
            error(errorCode, "update", result);
        else dds->powerdown(channel);
        return;
    }

    else if(0 == strcmp(pos, "*IDN?"))
    {
        identity(result);
        return;

    }

    error(10, "process", result);
    return;
}
