#ifndef DDSCOMMANDS_H
#define DDSCOMMANDS_H
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ddsDevice.h"
void error(int why, char *where, char *result);
int parseCommand(unsigned int *channel, double *parameter);
void identity(char * buffer);
void process(const char *command, char*result);
void dump(char* buf);
int regression_test();

#endif // DDSCOMMANDS_H
