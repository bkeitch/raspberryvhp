//needed for networking functionality
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//handle signals to exit gracefully
#include <signal.h>
volatile sig_atomic_t run = 1;

#include <stdio.h>
#include <cstdlib>
#include <string>
#include <string.h>
#include <iostream>
//handle errors nicely
#include <errno.h>
#include <unistd.h>
//shared memory for interprocess communication
#include <sys/ipc.h>
#include <sys/shm.h>
/** make a 1K shared memory segment */
#define SHM_SIZE 4


#ifdef DAC
extern "C" {
#include "dac/AD5371.h"
}
#endif //DAC

#ifdef DDS
#include "dds/ddsCommands.h"
#endif //DDS

///Keep track of which client has a lock on the system
static uint32_t* current_lock_owner_ip;
///define which port to listen on
const int LISTEN_PORT_NUMBER = 7000;
///define where to create a file to create memory key (ftok)
const char* memfile = "/tmp/listeningclient.shm";


#if !defined DDS && !defined DAC
/**
 * \brief Dummy process commane. Driver files should override this function and process the
given command.
 \param command the C-style string representing the received command
 \param result a charatcter pointer to the returned message.
*/
void process(const char *command, char*result)
{
    snprintf(result, 100, "You requested command : %s", command);

}
#endif

/**
 * \brief create a lock to prevent other connections modifying hardware settings
\param sockaddr_in the incoming socket address, to establish the client's IP
 */
void lock(struct sockaddr_in* sock)
{
#ifdef DEBUG
    std::cerr << " Locking " << sock->sin_addr.s_addr << std::endl;
#endif
    *current_lock_owner_ip = sock->sin_addr.s_addr;
}
/**
 * unlocks us to allow other clients access
 */
void unlock()
{
#ifdef DEBUG
    std::cerr << " Unlocking " << *current_lock_owner_ip << std::endl;
#endif// DEBUG
    *current_lock_owner_ip = 0;
}
/**
 * test to see if client is the lock owner
 \param sockaddr_in the incoming socket address, to test against the owner of the lock
 \return true if the address matches

 */
bool is_lock_owner(struct sockaddr_in* sock)
{
#ifdef DEBUG
    std::cerr << " Lock checking " << sock->sin_addr.s_addr << " against : " <<*current_lock_owner_ip<< std::endl;
#endif// DEBUG
    return (*current_lock_owner_ip == sock->sin_addr.s_addr );
}
/**
 \brief check if locked
 \return true if lock exists
  */
bool is_locked()
{
    return (*current_lock_owner_ip != 0 );
}

/**
 * \brief return error message if we have an invalid lock owner
 \param sockaddr_in the incoming socket address, to establish the client's IP for locking and unlocking the hardware
 \return an error message containing the current lock owner's address to allow client to see who has the lock.
 */
std::string invalid_lock_owner(struct sockaddr_in* sock)
{
    struct in_addr lock;
    lock.s_addr = *current_lock_owner_ip;
    char lock_ip[17];
    inet_ntop(AF_INET, &lock, lock_ip, 16);
    char your_ip[17];
    inet_ntop(AF_INET, &(sock->sin_addr), your_ip, 16);
    std::cerr << "System is locked by : "
              << lock_ip << " request from : " << your_ip <<std::endl;
    return std::string("LOCKED");
}
/**
 * \brief process the command looking for lock first. Also handles unlocking with either a LOCK_RESET
 or forced unlocking FORCE_LOCK_RESET. This is so that a client can override a lock that is left
 dangling if the client crashes. If the lock is OK, hand off to the driver
 \param sockaddr_in the incoming socket address, to test if this client has permission (is not locked out)
 \return string from the called command.

 */
std::string process_command(const std::string &command, struct sockaddr_in* cli_addr)
{
    if(command.empty())
    {
        return std::string("No Command supplied");
    }
    /// We can force an UNLOCK even if we are not the owner of the lock
    if(command.compare("FORCE_LOCK_RESET") == 0)
    {
        unlock();
        return std::string("Forced Unlock OK");
    }
    /// We check we are the lock owner before allowing any other commands
    if(!is_lock_owner(cli_addr) )
    {
        return invalid_lock_owner(cli_addr);
    }

    /// We can do a reset on the lock. Otherwise the command is handed to the h/w driver
    if(command.compare("LOCK_RESET") == 0)
    {
        unlock();
        return std::string("OK");
    }
    /// When the h/w driver returns, it should return a success or failure message which is returned as a string

    char* driver_result = (char*) malloc(100);
    process(command.c_str(), driver_result);
    std::string result = std::string(driver_result);
    free(driver_result);
    #ifdef DEBUG
    printf("**%s**\n", result.c_str());
    #endif // DEBUG

    return result;

}
/**
 \brief diagnostic dump of incoming TCP packet in hex. Formatted in 8 byte chunks.
 \param char* p the data coming in as a byte array
 \param len the length of the message. This is to allow just a few bytes to be dumped.
 We assume the packet is in blocks of 8. This function is NOT SAFE. It will cause pointer
 errors if used incorrectly and is only for debugging
*/
void dump(char * p, unsigned len)
{
    for (unsigned int byteNum = 0; byteNum <len; byteNum+=8)
    {
        printf("%03d: %02X%02X%02X%02X%02X%02X%02X%02X : %c%c%c%c%c%c%c%c :\n"
               , byteNum
               , p[byteNum+0]&0xFF
               , p[byteNum+1]&0xFF
               , p[byteNum+2]&0xFF
               , p[byteNum+3]&0xFF
               , p[byteNum+4]&0xFF
               , p[byteNum+5]&0xFF
               , p[byteNum+6]&0xFF
               , p[byteNum+7]&0xFF
               , std::max(p[byteNum+0]&0xFF, 32)
               , std::max(p[byteNum+1]&0xFF, 32)
               , std::max(p[byteNum+2]&0xFF, 32)
               , std::max(p[byteNum+3]&0xFF, 32)
               , std::max(p[byteNum+4]&0xFF, 32)
               , std::max(p[byteNum+5]&0xFF, 32)
               , std::max(p[byteNum+6]&0xFF, 32)
               , std::max(p[byteNum+7]&0xFF, 32)
              );
    }
}
/**
 \brief handle EXIT_CHLD signals nicely.
 \param incoming signal
*/
void exit_child (int sig)
{
    int w, status;
    do {
        w = waitpid(-1, &status, WNOHANG);
            if (w <= 0) {
            perror("waitpid");
            exit(EXIT_FAILURE);
        }
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
}
/**
 \brief handle SIGINT and make sure sockets are closed. Sets run (which is a global signal type) to 0
 prevent loop from running.
 \param sig not used.
*/
void exit_main_loop (int sig)
{
    run = 0;
}
/**
 \brief this is the worker thread that deals with the connection once it is spawned from the listener thread.

It takes the established socket and collects the data into a buffer. Once this has been done, it calls process_message to then do the work with this c-string. \see process_message.
\param newsockfd the established socket
\param sockaddr_in the incoming socket address, to establish the client's IP for locking and unlocking the hardware
*/
int process_msg(int newsockfd, struct sockaddr_in cli_addr)
{
    //the message storing strings
    char *incoming_message;
    std::string	command, return_message;
    ///First read size of incoming message
    uint8_t message_size = 0;
    read(newsockfd, &message_size, sizeof(message_size));
    #ifdef DEBUG
    fprintf(stderr, "Expecting: * %d *\n", (uint) message_size );
    #endif // DEBUG
    incoming_message = (char*) malloc (message_size+1);
    #ifdef DEBUG
    for (uint j = 0; j < message_size ; j++) incoming_message[j] = '_';
    #endif // DEBUG

    if (incoming_message==NULL) {
        perror("Memory allocation error");
        exit(EXIT_FAILURE);
    }


    read(newsockfd, incoming_message, message_size);
    //Teraterm etc. send a \\r others send \\n so we remove this.
    //if(incoming_message[message_size] == '\r')
    //Even though we convert to C++ strings, this is safer with memory
    incoming_message[message_size]='\0';

    #ifdef DEBUG
    dump(incoming_message, 8);
    #endif // DEBUG
    command = std::string(incoming_message);
    #ifdef DEBUG
    fprintf(stderr, "Received: * %s *\n", incoming_message);
    #endif // DEBUG
    free(incoming_message);
    ///Before processing the message, we check if we have a lock
    if(!is_locked())
    {
        ///current client now has lock
        lock(&cli_addr);
    }
    ///Then we check if we are the lock owner and if so, send command to hardware
    return_message = process_command(command, &cli_addr);
    #ifdef DUMP
    dump(incoming_message, message_size);
    #endif
    //Append the return message size to the beginning of the message
    uint8_t size_of_message = 0;
    size_of_message = static_cast<uint8_t>(return_message.size());
    char* output_buffer = (char*) malloc(size_of_message + 2);
    output_buffer[0] = size_of_message;
    output_buffer[1] = '\0';
    strncat(output_buffer, return_message.c_str(), size_of_message);

    #ifdef DEBUG
    fprintf(stderr, "Responded: * %s *\n", return_message.c_str());
    #endif // DEBUG

    //Send the return message (from the hardware driver)

    if (write(newsockfd, output_buffer, size_of_message+1 ) < 0)
    {
        perror("Error sending data");
        return errno;
    }
    free(output_buffer);
    #ifdef DEBUG
    fprintf(stderr, "Exiting child\n");
    #endif // DEBUG
    return EXIT_SUCCESS;

}
/**
\brief Starts a listening socket and hands over to a child process to handle connection.

This function uses the fork method to launch a separate thread. This keeps the system responsive to incoming requests.
However, by using shared memory, a lock is established that ensures only one client at a time can access hardware on the
device. The second connection will get an error message explaining that it does not have the lock. The first connected client
can then release the lock.

It relies on writing to a shared memory file and assumes it can write this to /tmp.
This is not portable to non-UNIX like systems
*/
int main()
{
    //shared memory segment for IPC
    key_t key;
    int shmid;
    //sockets for the created socket and connections
    int sockfd, newsockfd, ret;
    pid_t pid;
    //length of the client addr
    socklen_t clilen;
    //structures for various information about the client and server
    struct sockaddr_in serv_addr, cli_addr;
    /* make sig int clean up dangling connections: */
    struct sigaction sigIntHandler;
    struct sigaction sigChldHandler;
    sigChldHandler.sa_handler = exit_child;
    sigemptyset(&sigChldHandler.sa_mask);
    sigChldHandler.sa_flags = SA_RESTART | SA_NOCLDSTOP;
    if (sigaction(SIGCHLD, &sigChldHandler, 0) == -1) {
        perror("sigaction");
        exit(1);
    }
    sigIntHandler.sa_handler = exit_main_loop;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;

    sigaction(SIGINT, &sigIntHandler, NULL);
    FILE* temp;
    temp = fopen(memfile, "a+");
    if(errno || (NULL == temp))
    {
        perror("Could not open /tmp/listeningclient.shm lock file");
        return errno;
    }

    if(errno || (fclose(temp) != 0))
    {
        perror("Could not close /tmp/listeningclient.shm lock file");
        return errno;
    }
    if ((key = ftok(memfile, '1')) == -1)
    {
        perror("could not create key from /tmp/listeningclient.shm");
        return errno;
    }

    /* connect to (and possibly create) the segment: */
    if ((shmid = shmget(key, SHM_SIZE, 0644 | IPC_CREAT)) == -1)
    {
        perror("could not create shared memory segment: ");
        return errno;
    }

    /* attach to the segment to get a pointer to it: */
    current_lock_owner_ip = (uint32_t *) shmat(shmid, (void *)0, 0);
    if (current_lock_owner_ip == (uint32_t *)(-1))
    {
        perror("could not set data in shared memory segment: ");
        return errno;
    }

    //Create Socket, if fail, exit from program
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
    {
        printf("Error building Socket FD: ");
        return errno;
    }

    //Reset memory
    memset((void *) &serv_addr, 0, (size_t)sizeof(serv_addr));
    //Set up our server information
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    //Assign the port number from assigned port above
    serv_addr.sin_port = htons(LISTEN_PORT_NUMBER);
    int reuseaddr = 1;
    if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&reuseaddr,sizeof(reuseaddr)) < 0) {
        perror("Failed to reuse listening socket: ");
        return errno;
    }
    //bind socket
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("Error in Binding");
        return errno;
    }

    //Set up the socket to listen
    if (listen(sockfd, 5) < 0)
    {
        printf("Error listening");
        return errno;
    };

    /// The listening loop is only broken by signal HUP or higher
    printf("Ready to accept connections %u \n", LISTEN_PORT_NUMBER);
    while(run==1)
    {
        //set the clilen variable prior to passing to accept.
        //per the accept man page (explaining why I am doing this)
        //it should initially contain the size of the structure pointed
        //to by addr; on return it will contain the actual length (in bytes)
        //of the address returned. This would fail if clilen was null
        clilen = sizeof(cli_addr);
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen );

        if (newsockfd < 0)
        {
            perror("Connection Error");
            return errno;
        }
        pid=fork(); // spawn a child to handle request, and carry on listening
        if( pid == 0) //child has PID 0
        {
            ret = process_msg(newsockfd, cli_addr); //handle request
            exit(ret); //killl the child
        }
        close(newsockfd); //we're done so close the requests socket
    }
    /* detach from the segment: */
    if (shmdt(current_lock_owner_ip) == -1)
    {
        perror("Removing memory");
        return errno;
    }
    if (shmctl(shmid, IPC_RMID, NULL) == -1)
    {
        perror("Deleting memory");
        return errno;
    }
    if (close(sockfd))
    {
        perror("Closing socket");
        return errno;
    }
    return(EXIT_SUCCESS);

}
