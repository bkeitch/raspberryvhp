#define firmwareVerNum 2.0  //BCK 2012.07.09
#define MAX_CHANNEL 40 //BCK 2012.07.09

/** AD5372 -- Firmware driver for Analog Devices AD5372 DACs.
 * Accepts text commands and reads/writes appropriate commands to/from the SPI bus to control the DAC.
 * Commands should be terminated with a single <LF> character. Any excess whitespace is ignored.
 *
 * Commands are:
 * Write Voltage -- "V <Channel> <Volts><LF>" ("Channel" is an unsigned int, "Volts" is a double. DAC value will not update until UPDATE command sent)
 * Write Voltage and Update -- "VU <Channel> <Volts><LF>
 * Query Voltage -- "V? <Channel><LF>" (returns the voltage set on channel in the form "V <Voltage><LF>")
 *
 * Set gain -- "M <Channel> <gain><LF> ("gain" is an unsigned short specifying a value for the gain register)
 * Set gain and update "MU <Channel><LF>
 * Query gain -- "M? <Channel><LF> (returns "M <gain><LF>")
 *
 * Set offset -- "C <Channel> <offset><LF> ("offset" is an unsigned short specifying a value for the offset register)
 * Set offset and Update -- "CU <Channel> <offset><LF>"
 * Query offset -- "C? <Channel><LF>" (returns "C <offset><LF>")
 *
 * Set offset DAC -- "O <DAC> <offset><LF>" (set offset DAC (DAC must be 0 or 1))
 * Set offset DAC and update -- "OU <DAC> <offset><LF>"
 * Query offset DAC -- "O? <DAC><LF>" (returns "O <offset><LF>")
 *
 * Update DAC -- "UPDATE<LF>" (Pulse LDAC line to update DAC value)
 * Identity -- "*IDN?<LF>" -- Prints a version string to the USB-serial bus
 *
 * N.B. Query commands return the value in the DAC register, not the value set on the DAC.
 * Thus, if a value is waiting to be updated then this value will be returned by the query, not the current DAC value...
 * N.B. Channel is 0->39
 *
 * N.B. A few DAC functions are currently unsupported:
 * Grouping -- the DAC supports writing to groups of channels (either to all channels in a group or the same channel in each group)
 * The DAC has a reset function which performs a power on reset.
 * The DAC has a clear function, which shorts the outupts to signal ground. N.B. the datasheet recommends using this to avoid glitches when changing the offset so we should probaby be careful about this...
 * The DAC has two registers which you can write data to, this is useful if you want to switch between two states.
 * The DAC can be programmed to have a thermal shutdown.
 *
 * Implementation -- The DAC uses the following settings:
 * Bit ordering: big-endian (MSB first)
 * Clock phase: Data read on falling edge, propagated on rising edge (CPHA=1)
 * Clock polarity: Clock low when idle (CPOL=0)
 * Read/write frame while SS LOW
 * Clock speed: 125kHz (kept low because of reflections from poor wiring...)
 * To get a symmetric voltage range, we set the offset DACs to 8192, so that when the voltage register is at
 * MAX_CHANNEL768 (half way through its range) we get 0V.
 *
Pins used on Rasberry PI (Version 1.0) :
see http://elinux.org/Rpi_Low-level_peripherals#General_Purpose_Input.2FOutput_.28GPIO.29

P1-19 	GPIO10 		SPI0_MOSI
P1-21 	GPIO9 		SPI0_MISO
P1-23 	GPIO11 		SPI0_SCLK
P1-24 	GPIO08 		SPI0_CE0_N
P1-26 	GPIO07 		SPI0_CE1_N
P1-25 	GND

LDAC 9                           // Used to update the DAC (DACs updated when LDAC is taken low)
*/
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AD5371.h"
#include "../spi/transfer.h"
#ifdef REGRESS_TEST
#define DEBUG
void transfer(byte* data, byte* b, size_t i)
{
    printf("%02X%02X%02X\n", data[0]&0xFF, data[1]&0xFF, data[2]&0xFF);
}
#endif //REGRESS_TEST
const char terminators []  = " \t\f\v\r\n";
/**
 \brief Prints out an error, given an error code
 \param why error code
 \param where callling function name
 \param reason the error message
*/
void error(int why, const char *where, char* reason)
{
    if(0==why) return;
    const char *reasons[] =
    {
        "",
        "channel out of range",
        "no set parameter specified",
        "unrecognised argument",
        "unexpected garbage at EOF",
        "parameter is outside DAC range",
        "read timeout",
        "clock overflow during read",
        "buffer overflow during read",
        "no command found",
        "unrecognised command",
        "unrecognised channel"
    };
    snprintf(reason, 100, "Error : %s : %s \n", where, reasons[why]);
}



/**
 * \brief parses command and returns 0 if no error, or an error code otherwise
    \param channel channel number
    \param parameter an (optional) parameter for the setVoltage, setGain etc commands
    \return error code
 */
unsigned parseCommand(char* commandStr, char* command, unsigned int *channel, double *parameter)
{
    if(NULL == commandStr)
    {
        return no_command_found;
    }
    char tempBuffer[strlen(command)];
    char *pos = strtok(commandStr, terminators);
    if(NULL == pos)
    {
        return no_command_found;
    }
    *command = *pos;
    pos = strtok(NULL, terminators);
    if (NULL == pos)
    {
        return(unrecognised_channel);
    }
    if(1 != sscanf(pos, "%u%s", channel, tempBuffer))    // Extra argument checks for remaining characters (e.g. will give an error if we pass "2a" as a voltage)
    {
        return(unrecognised_argument);
    }
    if(MAX_CHANNEL <= *channel)                                    // N.B. First DAC is 0
    {
        return(channel_out_of_range);
    }
    pos = strtok(NULL, terminators);
    if(NULL == pos)
    {
        return(no_set_parameter_specified);
    }
    if(1 != sscanf(pos, "%lf%s", parameter, tempBuffer))    // Extra argument checks for remaining characters (e.g. will give an error if we pass "2a" as a voltage)
    {
        return(unrecognised_argument);
    }

    pos = strtok(NULL, terminators);
    if(NULL != pos)
    {
        return(unexpected_garbage_at_EOF);
    }
    return(0);
}
/**
 \brief sets voltage of one channel
 \param channel channel to set
 \param voltage voltage to set in volts
 */
unsigned setVoltage(unsigned int channel, double voltage)
{

    byte group = (channel/8);                            // DAC divides channels into groups
    byte channelInGroup = (channel%8);                   // Calculate address of channel based on its group
    byte channelMask = ((group+1) << 3) | channelInGroup;
    byte mask = channelMask | AD5371_DAC_WRITE;

    double dacSetting = (double) (voltage /(vRef * DIVIDER) * AD5371_FULL_RANGE + AD5371_OFFSET_CODE);

    if(16384 < dacSetting || 0 > dacSetting) //2^14
    {
        return(parameter_is_outside_DAC_range);
    }
#ifdef DEBUG
    printf("Setting voltage : %f \n",voltage);
#endif // DEBUG

    unsigned short INPUT_CODE = (unsigned short)dacSetting;
    unsigned short SHIFTED_CODE = INPUT_CODE << 2;
    byte data[3];
    byte returnd[3];
    data[0] = mask;
    data[1] = ((byte*)&SHIFTED_CODE)[1];
    data[2] = ((byte*)&SHIFTED_CODE)[0];
    transfer(data, returnd, 3);
    return(0);
}
/**
    \brief return the voltage set in the current register for given channel
    \param channel channel
    \param double voltage in volts
    \return errorcode or 0 if no error.
*/
unsigned getVoltage(unsigned int channel, double *voltage)
{

    unsigned short channelMask = (8 +  channel) << 7;
    unsigned short command = channelMask | AD5371_READOUT_VOLTAGE_MASK;
    byte data[3];
    byte rdata[3] = {0,0,0};
    data[0] = ((byte)AD5371_READOUT);
    data[1] = (((byte*)&command)[1]);
    data[2] = (((byte*)&command)[0]);
    transfer(data, rdata, 3);
    data[0] = 0;
    data[1] = 0;
    data[2] = 0;
    transfer(data, rdata, 3);
    // Ignore first byte and read second two as an unsigned short
    uint16_t DAC_CODE = rdata[1] + 256*rdata[2];
    *voltage = (4*vRef) * (((double)DAC_CODE - AD5371_OFFSET_CODE)/AD5371_FULL_RANGE);   // Convert into a Voltage
    return 0;

}
/**
 \brief sets the offset of the DAC output. Norminally this is set to half the range
 for symmetric outputs
 \param channel the channel to set the offset on
 \param offset amount to offset by (in volts)
 */
unsigned setOffset( unsigned int channel, double offsetD)
{
    unsigned short offset = (unsigned short) offsetD;
    byte group = (channel/8);                            // DAC divides channels into groups
    byte channelInGroup = (channel%8);                   // Calculate address of channel based on its group
    byte channelMask = ((group+1) << 3) | channelInGroup;
    byte mask = channelMask | AD5371_OFFSET_WRITE;

    byte data[3];
    byte rdata[3] = {0,0,0};
    data[0] = (mask);
    data[1] = (((byte*)&offset)[1]);
    data[2] = (((byte*)&offset)[0]);
    transfer(data, rdata, 3);
    return 0;
}
/**
 \brief gets the current value of the offset in the register
 \param the offset value
 */
unsigned getOffset(unsigned channel, unsigned *offset)
{
    unsigned short channelMask = (8 +  channel) << 7;
    unsigned short command = channelMask | AD5371_READOUT_OFFSET_MASK;
    byte data[3];
    byte bytes[3];
    data[0] = ((byte)AD5371_READOUT);
    data[1] = (((byte*)&command)[1]);
    data[2] = (((byte*)&command)[0]);
    transfer(data, bytes, 3);
    data[0] = (0);
    data[2] = (0);
    data[1] = (0);
    transfer(data, bytes, 3);
    // Ignore first byte and read second two as an unsigned short
    *offset = *((unsigned short*)&bytes[1]);
    return 0;
}
/**
 \brief sets the gain on the channel
 \param channel channel to set
 \param gain
 \return 0 or errorcode
 */
unsigned setGain(unsigned int channel, double gaind)
{
    unsigned short gain = (unsigned short) gaind;

    byte group = (channel/8);                            // DAC divides channels into groups
    byte channelInGroup = (channel%8);                   // Calculate address of channel based on its group
    byte channelMask = ((group+1) << 3) | channelInGroup;
    byte mask = channelMask | AD5371_GAIN_WRITE;

    byte data[3];
    byte rdata[3] = {0,0,0};
    data[0] = (mask);
    data[1] = (((byte*)&gain)[1]);
    data[2] = (((byte*)&gain)[0]);
    transfer(data, rdata, 3);
    return 0;
}
/**
 \brief gets the gain for a particular channel
 \param channel
 \param gain
 \return errorcode
 */
unsigned getGain(unsigned channel, unsigned* gain)
{

    unsigned short channelMask = (8 +  channel) << 7;
    unsigned short command = channelMask | AD5371_READOUT_GAIN_MASK;

    byte data[3];
    byte bytes[3];
    data[0] = ((byte)AD5371_READOUT);
    data[1] = (((byte*)&command)[1]);
    data[2] = (((byte*)&command)[0]);
    transfer(data, bytes, 3);
    data[0] = (0);
    data[2] = (0);
    data[1] = (0);
    transfer(data, bytes, 3);
    *gain = *((unsigned short*)&bytes[1]);                    // Ignore first byte and read second two as an unsigned short
    return 0;
}
/**
 \brief sets the value of one of the 3 global offset DACS
 \param DACNumber 1, 2 or 3
 \param offsetd to set
 \return errorcode
 */
unsigned setOffsetDAC(unsigned DACNumber, double offsetd)
{
    unsigned short offset = (unsigned short) offsetd;
    byte data[3];
    byte rdata[3] = {0,0,0};
    if(0 == DACNumber)
        data[0]=(byte)AD5371_OFFSETDAC0_WRITE;
    else if(1 == DACNumber)
        data[0]=(byte)AD5371_OFFSETDAC1_WRITE;
    else if(2 == DACNumber)
        data[0]=(byte)AD5371_OFFSETDAC2_WRITE;
    data[1] = (((byte*)&offset)[1]);
    data[2] = (((byte*)&offset)[0]);
    transfer(data, rdata, 3);
    return 0;
}

unsigned getOffsetDAC(unsigned DACNumber, unsigned* offset)
{
    unsigned short command;

    if (0 == DACNumber)
        command = AD5371_READOUT_OFFSETDAC0_MASK;
    else if(1 == DACNumber)
        command = AD5371_READOUT_OFFSETDAC1_MASK;
    else if(2 == DACNumber)
        command = AD5371_READOUT_OFFSETDAC2_MASK;
    byte data[3];
    byte bytes[3] = {0,0,0};
    data[0] = ((byte)AD5371_READOUT);
    data[1] = (((byte*)&command)[1]);
    data[2] = (((byte*)&command)[0]);
    transfer(data, bytes, 3);
    bytes[0] = (0);
    data[1] = (0);
    data[2] = (0);
    transfer(data, bytes, 3);
    *offset = *((unsigned short*)&bytes[1]);                    // Ignore first byte and read second two as an unsigned short
    return 0;
}

unsigned pulseLDAC()
{
    return 0;
    /*//digitalWrite(LDAC, LOW);                   // Pulse the LDAC channel to update the DAC
    //delayMicroseconds(10);
    //digitalWrite(LDAC, HIGH);
    */
}
/**
 \brief identify firmware version of the driver
*/
void identity(char* buffer)
{
    char *pos = strtok(NULL, terminators);
    if('\0' != pos)
    {
        error(unexpected_garbage_at_EOF, "identity()", buffer);
        return;
    }
    sprintf(buffer, "AD5371 firmware version V%1.1f \n", firmwareVerNum);

}
/**
 * switches off all DACS
 */
void off()
{
    byte data[3] = {1,0,1};
    byte rdata[3] = {0,0,0};
    transfer(data, rdata, 3);
}
/**
 * switches on all DACS
 */
void on()
{
    byte data[3] = {1,0,0};
    byte rdata[3] = {0,0,0};
    transfer(data, rdata, 3);
}
/**
 re-initialises by switch off then on, then sets all the offset DACS to the value in AD5371_OFFSET_CODE
 the DAC chip
 */
void initOffset()
{
    byte data[3] = {1,0,1};
    byte rdata[3] = {0,0,0};
    transfer(data, rdata, 3); //off
    data[2] = 0;
    transfer(data, rdata, 3); //on

    unsigned short offset = AD5371_OFFSET_CODE;
    data[0] = ((byte)AD5371_OFFSETDAC0_WRITE);
    data[1] = (((byte*)&offset)[1]);
    data[2] = (((byte*)&offset)[0]);
    transfer(data, rdata, 3);
    data[0] = ((byte)AD5371_OFFSETDAC1_WRITE);
    data[1] = (((byte*)&offset)[1]);
    data[2] = (((byte*)&offset)[0]);
    transfer(data, rdata, 3);
    data[0] = ((byte)AD5371_OFFSETDAC2_WRITE);
    data[1] = (((byte*)&offset)[1]);
    data[2] = (((byte*)&offset)[0]);

    transfer(data, rdata, 3);
}
/** Program main loop
 \param command command to process in the form L 0 1.2 where L is the command (1-3 letters)
 0 is the channel (0-39) and the third optional value is a value to set such as voltage or offset
 */
void process(const char* commandStr, char* result)
{
    unsigned int channel = 0;
    double param = 0.0;
#ifdef DEBUG
    printf("\n* Executing command: %s*\n", commandStr);
#else
    ///Initialise the SPI comms.
    init();
#endif
    char command[4];
    strcpy(result, "OK");
    int errorCode = parseCommand((char*)commandStr, command, &channel, &param);
    if(errorCode != 0)
    {
        error(errorCode, "parseCommand()", result);
        return;
    }
    char baseCommand = command[0];
    #ifdef DEBUG_CMD
    printf("Command %s %d %f\n", command, channel, param);
    #endif // DEBUG_CMD
    switch (baseCommand)
    {
    case 'R':
        initOffset();
        break;
    case 'F':
        off();
        break;
    case 'P':
        on();
        break;
    case 'U':
        pulseLDAC();
        break;
    case 'V':
        if(0 == strcmp(command, "V?"))
        {
            double voltage;
            errorCode = getVoltage(channel, &voltage);
            sprintf(result, "V %u %f", channel, voltage);
        } else {
            errorCode = setVoltage(channel, param);
        }
        if(errorCode !=0)
        {
            error(errorCode, "setVoltage()", result);
        }
        break;
    case 'C':
        if (0 == strcmp(command, "C?"))
        {
            unsigned offset;
            errorCode = getOffset(channel, &offset);
            sprintf(result, "C %u %u", channel, offset);
        } else {
            errorCode = setOffset(channel, param);
        }
        if(errorCode !=0)
        {
            error(errorCode, "setOffset()", result);
        }
        break;
    case 'M':
        if (0 == strcmp(command, "M?"))
        {
            unsigned gain;
            errorCode = getGain(channel, &gain);
            sprintf(result, "M %u %u", channel, gain);
        } else {
            errorCode = setGain(channel, param);
        }
        if(errorCode !=0)
        {
            error(errorCode, "setGain()", result);
        }
        break;
    case 'O':
        if (0 == strcmp(command, "O?"))
        {
            unsigned offset;
            errorCode = getOffsetDAC(channel, &offset);
            sprintf(result, "O %u %u", channel, offset);
        } else {
            errorCode = setOffsetDAC(channel, param);
        }
        if(errorCode !=0)
        {
            error(errorCode, "setOffsetDAC()", result);
        }
        break;
    default:

        if(0 == strcmp(command, "*IDN?"))
        {
            identity(result);

        } else {
            error(unrecognised_command, "process()", result);
        }
    }//switch
    if(0 == strcmp(command, "U"))
        pulseLDAC();
}
#ifdef REGRESS_TEST

/**
    Read at most 'length'-1 characters from the file 'f' into
        'buf' and zero-terminate this character sequence. If the
        line contains more characters, discard the rest.
        \param buf to read into
        \param f file handle to read from
   */
char *read_line (char *buf, FILE *f)
{
    size_t length = 12;
    char *p;

    p = fgets (buf, length, f);
//    size_t last = strlen (buf) - 1;
//    if(p) {
//        if (buf[last] == '\n') {
//            // Discard the trailing newline
//            buf[last] = '\0';
//        } else {
//            // There's no newline in the buffer, therefore there must be
//            //     more characters on that line: discard them!
//
//            fscanf (f, "%*[^\n]");
//            // And also discard the newline...
//            (void) fgetc (f);
//        } // end if
//    }   // end if
    return p;
}
void dump(char* buf)
{
    int i = 0;
    for (i=0; i<12; i++)
    {
        printf("%X ", buf[i]);
    }
    printf("\n");
}
/**
 Performs a regression test by reading in commands from file
 reg_test.data
 these can be outputted and compared to
 reg_test.results
 */
int main()
{
    char command[12];
    /*strcpy(command, "*IDN? \n");
    process(command);

    strcpy(command,"M? 1\n");
    process(command);

    strcpy(command, "V? 1\n");
    process(command);

    strcpy(command, "V 1 5.0\n");
    process(command);

    strcpy(command, "V? 1\n");
    process(command);

    strcpy(command, "M 2 0.5\n");
    process(command);

    strcpy(command, "M? 2\n");
    process(command);

    strcpy(command, "O 1 1.0\n");
    process(command);

    strcpy(command, "VU 2 3.0\n");
    process(command);

    strcpy(command, "V? 2\n");
    process(command);
    */
    char* buffer= (char*) malloc(100);
    FILE *f = fopen("reg_test.data", "r");
    while (!feof(f) && command != NULL && strcmp(command, "\n") !=0)
    {
        read_line(command, f);
        //printf("[%s]", command);
        process(command, buffer);
        printf("%s\n", buffer);
    }
    free(buffer);
    fclose(f);
    return 0;
}
#endif //REGRESS_TEST
