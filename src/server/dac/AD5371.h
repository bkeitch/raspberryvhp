#ifndef AD9371_H
#define AD9371_H
// DAC commands
#define AD5371_DAC_WRITE               0xC0
#define AD5371_OFFSET_WRITE            0x80
#define AD5371_GAIN_WRITE              0x40
#define AD5371_OFFSETDAC0_WRITE        0x02
#define AD5371_OFFSETDAC1_WRITE        0x03
#define AD5371_OFFSETDAC2_WRITE        0x04
#define AD5371_READOUT                 0x05
#define AD5371_READOUT_GAIN_MASK       0x6000
#define AD5371_READOUT_VOLTAGE_MASK    0x0000    // By default the DACs use the A register, so this is the one we query
#define AD5371_READOUT_OFFSET_MASK     0x4000
#define AD5371_READOUT_OFFSETDAC0_MASK 0x8100
#define AD5371_READOUT_OFFSETDAC1_MASK 0x8180
#define AD5371_READOUT_OFFSETDAC2_MASK 0x8200
#define AD5371_FULL_RANGE 0x4000
#define DIVIDER 4 // FOR AD5372 THIS IS 4
// DAC settings
#define vRef 5

#define AD5371_OFFSET_CODE 8192           // used to convert DAC codes <=> voltages AND max for 5V - see datasheet page 17

// Buffering settings (don't use too large a buffer or you'll run out of RAM!)
#define maxNumChars 254                     // Size of buffer to use when reading commands from USB serial bus (don't make too large or we run out of RAM!)
#define readTimeout 100                    // Timeout (ms) to use for reading from serial port in ms N.B. this is the maximum time to wait between chars (not a limit on the total read time)

#define channel_out_of_range (1)
#define no_set_parameter_specified (2)
#define unrecognised_argument (3)
#define unexpected_garbage_at_EOF (4)
#define parameter_is_outside_DAC_range (5)
#define read_timeout (6)
#define clock_overflow_during_read (7)
#define buffer_overflow_during_read (8)
#define no_command_found (9)
#define unrecognised_command (10)
#define unrecognised_channel (11)
typedef uint8_t byte;
void error(int why, const char* where, char* result);
unsigned parseCommand(char* commandStr, char* command, unsigned int *channel, double *parameter);
unsigned setVoltage(unsigned int channel, double voltage);
unsigned getVoltage(unsigned int channel, double *voltage);
unsigned setOffset(unsigned int channel, double offsetD);
unsigned getOffset(unsigned int channel, unsigned *offsetD);
unsigned setGain(unsigned int channel, double gain);
unsigned getGain(unsigned int channel, unsigned* gain);
unsigned setOffsetDAC(unsigned int channel, double offsetD);
unsigned getOffsetDAC(unsigned int channel, unsigned* offsetD);
unsigned pulseLDAC();
void identity(char * buffer);
void initOffset();
void process(const char *commandStr, char*result);
void dump(char* buf);
#endif //AD5371_H
