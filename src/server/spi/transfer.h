#ifndef TRANSFER_H
#define TRANSFER_H
#include <stdint.h>
int init();
void transfer (uint8_t* tx, uint8_t* rx, size_t size);

void cleanup() ;

#endif // TRANSFER_H
