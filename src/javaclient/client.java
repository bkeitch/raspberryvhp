/*  A Java socket test */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
/**
 * Test client for the raspberrypi VHP
 * Opens a socket and sends a request
*/
public class client {
    private static final String mHost = "127.0.0.1";
    private static final int mPort = 7000;
    public static void main (String args[]) {
	try {
	    Socket socket = new Socket(mHost, mPort);
	    PrintWriter out = new PrintWriter( socket.getOutputStream(), true);
	    BufferedReader in = new BufferedReader( new InputStreamReader(socket.getInputStream()));
            out.printf((char)10 + "LOCK_RESET");
	    String line = in.readLine();
	    System.out.println (line);
	    socket.close();

        } catch (UnknownHostException uhe) {
            System.out.println ("Unknown host : " + mHost);
            uhe.printStackTrace();

        } catch (IOException ioe) {
            System.out.println ("IO Exception : " + ioe);
            ioe.printStackTrace();

        }
	System.out.println ("Finished!");
   }
}
