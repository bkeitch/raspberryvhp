#!/usr/bin/env python

import socket
import struct

TCP_IP = '129.67.92.87'
TCP_PORT = 7000
#MESSAGE = "Hello This is a very much longer message"
MESSAGE = "V 10 7.554"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
s.send(bytes('' + chr(len(MESSAGE)) + MESSAGE, "utf-8"))
length = s.recv(1)
value = int.from_bytes(length,'big')
#value = struct.unpack('>i', b''length[0])[0]
data = s.recv(value)
s.close()

print ("received data:", data)
