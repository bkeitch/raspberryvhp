#-------------------------------------------------
#
# Project created by QtCreator 2014-03-19T09:36:49
#
#-------------------------------------------------

QT       += core gui network
CONFIG   += console
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = raspberryclient
TEMPLATE = app
#DEFINES += RECURSIVE_QT_LOCKS

INCLUDEPATH += ../../../ca-ionizer/ionizer2/ ../../../ca-ionizer/shared/src/

SOURCES += main.cpp\
        mainwindow.cpp \
    ../../../ca-ionizer/shared/src/TcpDevice.cpp \
    ../../../ca-ionizer/shared/src/raspberrypi.cpp \
    ../../../ca-ionizer/ionizer2/PiConnection.cpp \
    ../../../ca-ionizer/ionizer2/MessageExchange.cpp \
    ../../../ca-ionizer/ionizer2/Message.cpp

HEADERS  += mainwindow.h \
    ../../../ca-ionizer/shared/src/TcpDevice.h \
    ../../../ca-ionizer/shared/src/raspberrypi.h \
    ../../../ca-ionizer/ionizer2/Message.h \ 
    ../../../ca-ionizer/ionizer2/PiConnection.h \
    ../../../ca-ionizer/ionizer2/MessageExchange.h

FORMS    += mainwindow.ui
