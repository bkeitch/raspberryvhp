#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <PiConnection.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void _init();

public slots:
    void init();
    void update_1();
    void reset_1();
    void update_2();
    void reset_2();
    void update_3();
    void reset_3();
    void update_4();
    void reset_4();
    void updateF_1();
    void updateF_2();
    void updateF_3();
    void updateF_4();
    void release();
    void doCmd();

private:

    Ui::MainWindow *ui;
    PiConnection *pi;
    std::string ip_address;
    int port;

};

#endif // MAINWINDOW_H
