#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow())
{
    ui->setupUi(this);
    connect(ui->btnConnect, SIGNAL(released()),
        this, SLOT(init()));
    connect(ui->btnRelease, SIGNAL(released()),
        this, SLOT(release()));
    connect(ui->btnReset_1, SIGNAL(released()),
        this, SLOT(reset_1()));
    connect(ui->btnUpdate_1, SIGNAL(released()),
        this, SLOT(update_1()));
    connect(ui->btnReset_2, SIGNAL(released()),
        this, SLOT(reset_2()));
    connect(ui->btnUpdate_2, SIGNAL(released()),
        this, SLOT(update_2()));
    connect(ui->btnReset_3, SIGNAL(released()),
        this, SLOT(reset_3()));
    connect(ui->btnUpdate_3, SIGNAL(released()),
        this, SLOT(update_3()));
    connect(ui->btnReset_4, SIGNAL(released()),
        this, SLOT(reset_4()));
    connect(ui->btnUpdate_4, SIGNAL(released()),
        this, SLOT(update_4()));
    connect(ui->btnConnect_Go, SIGNAL(released()),
        this, SLOT(doCmd()));
    connect(ui->btnUpdateF_1, SIGNAL(released()),
        this, SLOT(updateF_1()));
    connect(ui->btnUpdateF_2, SIGNAL(released()),
        this, SLOT(updateF_2()));
    connect(ui->btnUpdateF_3, SIGNAL(released()),
        this, SLOT(updateF_3()));
    connect(ui->btnUpdateF_4, SIGNAL(released()),
        this, SLOT(updateF_4()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init() {
    this->port = ui->txtPort->displayText().toInt();
    this->ip_address = ui->txtIP->displayText().toStdString();
    if(this->port == 0) {
        QMessageBox mbx;
        mbx.setText("Invalid Port");
        mbx.show();
        return;
    }
    if(this->ip_address == "") {
        QMessageBox mbx;
        mbx.setText("Invalid IP");
        mbx.show();
        return;
    }
    this->pi = new PiConnection(this, this->ip_address, this->port);
    pi->start();
    pi->init();
}
void MainWindow::doCmd() {
    if(!pi) return;
    pi->doCommand(ui->txtCommand->text());

}

void MainWindow::release() {
    if(!pi) return;
    pi->releaseLock();
}

void MainWindow::reset_1() {
    if(!pi) return;
    ui->voltage_1->setValue(pi->getVoltage(1));
}
void MainWindow::reset_2() {
    if(!pi) return;
    ui->voltage_1->setValue(pi->getVoltage(2));
}
void MainWindow::reset_3() {
    if(!pi) return;
    ui->voltage_1->setValue(pi->getVoltage(3));
}
void MainWindow::reset_4()
{
    if(!pi) return;
    ui->voltage_1->setValue(pi->getVoltage(4));
}
void MainWindow::update_1()
{
    if(!pi) return;
    pi->setVoltage(1, ui->voltage_1->value());
}
void MainWindow::update_2()
{
    if(!pi) return;
    pi->setVoltage(2, ui->voltage_2->value());
}
void MainWindow::update_3()
{
    if(!pi) return;
    pi->setVoltage(3, ui->voltage_3->value());
}
void MainWindow::update_4()
{
    if(!pi) return;
    pi->setVoltage(4, ui->voltage_4->value());
}
void MainWindow::updateF_1()
{
    if(!pi) return;
    pi->doCommand(QString("F 1 ").append(ui->frequency_1->text()));
}
void MainWindow::updateF_2()
{
    if(!pi) return;
    pi->doCommand(QString("F 2 ").append(ui->frequency_2->text()));
}
void MainWindow::updateF_3()
{
    if(!pi) return;
    pi->doCommand(QString("F 3 ").append(ui->frequency_3->text()));
}
void MainWindow::updateF_4()
{
    if(!pi) return;
    pi->doCommand(QString("F 4 ").append(ui->frequency_4->text()));

}
