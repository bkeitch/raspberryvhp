Raspberry Pi VHP
================

This project enables the use of hardware attached to a Raspberry Pi (or similar embedded controller) via ethernet.

It creates a Virtual Hardware Port (VHP) that can be accessed via a text-based protocol.

It is possible to write a driver to use any of the output ports of the Raspberry Pi (SPI, USB or parallel custom)

Examples included are for a Analog Devices DAC chip that uses SPI and a DDS device that requires serial commands over USB.

Building
--------

To cross-compile use the following instructions:

[[http://hertaville.com/development-environment-raspberry-pi-cross-compiler.html#Downloading-and-Setting-Up-the-Cross-Compiling-Toolchain]]

or in summary:

* ensure you have build-essential, git and codeblocks installed:

    sudo apt-get install build-essential git codeblocks

* install the raspberry pi tools from the git repo:

    git clone https://github.com/raspberrypi/tools

* edit the Makefile to point to the correct ARM compiler

Authors: Ben Keitch (2015)
